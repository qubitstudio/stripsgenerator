# Bingo90 Ticket Generator

Generator of an arbitrary number of Bingo90 strips. Every strip is made of 6 tickets, each with 3 rows and 9 columns.

**Requirements:**

* Strip of 6 tickets
  - Tickets are created as strips of 6, because this allows every number from 1 to 90 to appear across all 6 tickets. If they buy a full strip of six it means that players are guaranteed to mark off a number every time a number is called.
* Each row contains five numbers and four blank spaces
* Each column contains up to three numbers, which are arranged as follows:
  - The first column contains numbers from 1 to 9 (or 10),
  - The second column numbers from 10 (or 11) to 20,
  - The third, 20 (or 21) to 30 and so on up until the last column, which contains numbers from 81 to 90.
* Each column should contain at least 1 number (and not 3 white spaces)
* There can be **no duplicate** numbers between 1 and 90 **in the strip** (since you generate 6 tickets with 15 numbers each)

**Start the app.**

`mvn clean install`

`mvn spring-boot:run -Dspring-boot.run.arguments="100"`

The argument specifies the number of strips to generate. Please be aware no check is done to be sure it's a positive number. If the parameter is omitted, it defaults to 100.

**How a strip is generated.**

1. A strip is represented by a 2d arrayof int: int[][]
2. The 1-90 numbers are added to a Map<Integer, List<Integer>>, where:
    * the key is the ticket column index;
    * the value is a list of numbers to be added in that column; it's shuffled to chose random numbers, and the index of boundaries values is chose randomly, e.g. 10 can be in key 0 or 1 based on a random boolean.
3. An array of int is used to keep track of how many numbers are currently in each row.
4. For every ticket (made of 3 columns) randomly chose one of the 3 rows, and add the first number. This is to satisfy the constrain of having at least 1 value for every column
5. Starting from a random index, we loop over the remaining column values. We check every row (still starting randomly) to fit the number.
6.  Since not all the numbers can find a place, we swap values between rows:
    1. Get all the rows with available space
    2. Get all completed rows where we could add the number (if they didn't reach the limit of 5 numbers)
    3. If one of the available rows have a zero, and in the same column the completed row have a value, we swap the values. This means that the available row will be complete (with 5 values), and the completed row will have a zero, freeing up space for the number to be added. Since we are adding a zero to the completed row column, we need to check that the column have at least another value, otherwise we will end up with a 3 zeroes column.
    4. Once founf the proper rows and values, we do the swap and we add the number.
    5. In case the conditions are not met, we dump the current strip and we try to create a new one instead. This happens less than 1/10th of the times.
    
    E.g. swappable conditions
    Assuming we need to add the number '6' (index 0)
                    
                    
    complete row: 0 X X X X X 0 0 0
    
    complete row: - - - - - 0 X
    
    complete row: - - - - - 0 0
                            
    available row - X X X X 0 0 0 0 0                         
                           
    Note how the index 4 could be chose because there is a value in complete row, and a 0 in available row, however the other 2 values of the column are zeros, meaning the column would have 3 zeroes after the swap
    
    The swappable index than is 5, after the swap:
    
    complete row  - 0 X X X X 0 0 0 0 <- now we can add the number with index 0 and have: 6 X X X X 0 0 0 0
    
    available row - X X X X 0 X 0 0 0
    
    The result is 2 completed columns
    
7. The last step is to order the values in every ticket column