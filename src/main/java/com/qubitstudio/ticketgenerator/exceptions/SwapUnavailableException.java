package com.qubitstudio.ticketgenerator.exceptions;

public class SwapUnavailableException extends Exception {
    public SwapUnavailableException(String message) {
        super(message);
    }
}
