package com.qubitstudio.ticketgenerator.generator;

import com.qubitstudio.ticketgenerator.exceptions.SwapUnavailableException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StripsGenerator {

    private List<int[][]> strips;

    private final int NUM_TICKETS = 6;
    private final int NUM_ROWS = 3;
    private final int NUM_COLUMNS = 9;
    private final Random random = new Random();

    public List<int[][]> generate(int numStrips) {
        return generate(numStrips, false);
    }

    /**
     * Generate a set number of strips made of 6 tickets, optionally print to console
     *
     * @param numStrips
     * @param print
     * @return
     */
    public List<int[][]> generate(int numStrips, boolean print) {

        strips = new ArrayList<>();

        for (int stripIndex = 0; stripIndex < numStrips; stripIndex++) { // Generate tickets

            try {
                int[][] strip = generateStrip(stripIndex);
                strips.add(strip);
                if (print) {
                    print(strip, stripIndex);
                }
            } catch (SwapUnavailableException e) { // Couldn't find any row fit for the value swap, dump current strip and try again
                if (print) {
                    System.out.println(e);
                }
                stripIndex--; // Try again
            }

        }

        return strips;
    }

    /**
     * Generate a strip as a bi-dimensional array made of 6 tickets 3x9
     *
     * @return
     * @throws SwapUnavailableException
     */
    private int[][] generateStrip(int stripIndex) throws SwapUnavailableException {

        // Create empty strip
        int[][] strip = new int[NUM_TICKETS * NUM_ROWS][NUM_COLUMNS];

        // Create numbers from 1 to 90 and index them by the index of the column they will be inserted. The index for bound values (e.g. 10 or 20) is generated randomly, so 10 can have either index 0 or 1
        Map<Integer, List<Integer>> numbersByColumn = createNumbersByColumn();
        // shuffle the values to randomize values chosen to be inserted
        for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
            Collections.shuffle(numbersByColumn.get(columnIndex));
        }

        // Keep track of how many values have been added to every row
        int[] numValuesInRow = new int[NUM_TICKETS * NUM_ROWS];

        // For every ticket (3 columns) randomly chose one of the 3 rows, and add the first number. This is to satisfy the constrain of having at least 1 value for every column
        fill1NumberPerColumn(strip, numbersByColumn, numValuesInRow);

        // Loop over the remaining numbers to be filled in, column by column. Shuffle column index so every strip starts from a random column position
        List<Integer> randomColumnIndexes = IntStream.range(0, 9).boxed().collect(Collectors.toList());
        Collections.shuffle(randomColumnIndexes);
        for (Integer colNumberIndex : randomColumnIndexes) {
            // Shuffle row index so every strip starts from a random row position
            List<Integer> randomRowIndexes = IntStream.range(0, 18)
                    .filter(rowIndex -> numValuesInRow[rowIndex] < 5 && strip[rowIndex][colNumberIndex] == 0) // Only keep available rows
                    .boxed().collect(Collectors.toList());
            Collections.shuffle(randomRowIndexes);

            for (Integer rowIndex : randomRowIndexes) {
                if (numbersByColumn.get(colNumberIndex).isEmpty()) { // this columns is done, break row loop and move to next column index
                    break;
                }
                strip[rowIndex][colNumberIndex] = numbersByColumn.get(colNumberIndex).remove(0); // Every number added is also removed, so we just have the available numbers
                numValuesInRow[rowIndex]++;
            }
        }

        // We try to add numbers that couldn't fit by searching a row with available space and swapping the values in the desired position
        randomColumnIndexes.stream().filter(columnIndex -> !numbersByColumn.get(columnIndex).isEmpty()).collect(Collectors.toList()); // Exclude columns where all numbers have been added already
        Collections.shuffle(randomColumnIndexes); // randomize columns again
        for (Integer colNumberIndex : randomColumnIndexes) {

            for (Integer numberToAdd : numbersByColumn.get(colNumberIndex)) {

                // Find rows with available space (less than 5 columns filled) to try to add a number from a complete column (swap)
                List<Integer> idRowsWithAvailableSpace = getIdRowsWithAvailableSpace(numValuesInRow);

                // Find a row available for the swap - completed but with an available spot for the number
                // In completed row, search column with filled value, at least another value in the column (otherwise the whole column will be made of zeroes after the swap), and a zero in the corresponding available row.
                // We then swap it, so:
                // 1) We fill the available row with the swapped value;
                // 2) We free up 1 place in the completed row
                // 3) We can add the number in the completed row, since 1 place was freed-up by the swap
                int swapRowIndex = -1;
                int swapColIndex = -1;
                int swapAvailableRowIndex = -1;
                boolean swappable = false;
                for (int rowIndex = 0; rowIndex < strip.length; rowIndex++) {
                    if (numValuesInRow[rowIndex] == 5 && strip[rowIndex][colNumberIndex] == 0) { // Find the first completed row available for the swap
                        // If there is a column with value and zero in the corresponding idRowWithAvailableSpace row column, && other 2 values are not both zeros (otherwise the whole column will be of zeroes), SWAP
                        for (int colIndex = 0; colIndex < strip[rowIndex].length; colIndex++) {
                            if (colIndex == colNumberIndex) {
                                continue;
                            }
                            for (Integer idRowAvailable : idRowsWithAvailableSpace) { // Try every row with available space
                                // Find a column in the current row with a non zero value and other non-zero values in the same column, and a zero value in the corresponding idRowWithAvailableSpace
                                if (strip[rowIndex][colIndex] != 0 && strip[idRowAvailable][colIndex] == 0 && hasColumnOtherNonZeroValues(strip, rowIndex, colIndex)) {
                                    swapRowIndex = rowIndex;
                                    swapColIndex = colIndex;
                                    swapAvailableRowIndex = idRowAvailable;
                                    swappable = true;
                                } // otherwise keep searching
                            }

                        }
                    }
                }

                if (swappable) {
                    // Swap values
                    strip[swapRowIndex][colNumberIndex] = numberToAdd; // Assign number to row, the row now has 6 values: swap the choosen one
                    strip[swapAvailableRowIndex][swapColIndex] = strip[swapRowIndex][swapColIndex];
                    strip[swapRowIndex][swapColIndex] = 0;
                    numValuesInRow[swapAvailableRowIndex]++;
                } else {
                    // This strip is dumped, a new one will be generated instead
                    throw new SwapUnavailableException("Strip with id: " + stripIndex + " need to be retried");
                }

            }
        }

        // Sort the values on every ticket column
        sortColumns(strip);

        return strip;
    }

    /**
     * Add at least 1 number to every column of every ticket - a ticket column (3 values) must have at least 1 number (and 0 zeroes), up to 3 numbers (and no zeroes)
     *
     * @param strip
     * @param numbersByColumn
     * @param numValuesInRow
     */
    private void fill1NumberPerColumn(int[][] strip, Map<Integer, List<Integer>> numbersByColumn, int[] numValuesInRow) {
        for (int ticket = 0; ticket < NUM_TICKETS; ticket++) {
            for (int col = 0; col < NUM_COLUMNS; col++) {
                int bingoRow;
                int randomTicketRow;
                do {
                    randomTicketRow = random.nextInt(NUM_ROWS);
                    bingoRow = ticket * NUM_ROWS + randomTicketRow;
                } while (numValuesInRow[bingoRow] >= 5); // If the row already reached the limit of 5 rows, generate another random row
                strip[bingoRow][col] = numbersByColumn.get(col).remove(0); // get the element and remove it from the list
                numValuesInRow[bingoRow]++; // Update counter of elements for this row
            }
        }
    }


    /**
     * Generate a map of <columnIndex, number>, where the edge number is included in one of the 2 possible indexes by a random factor,
     * e.g. 10 can either have index 0 or 1
     * <p>
     * false <- don't include the last available number, in this case '10'. This row will end with '9' while the next will start with '10'
     * [1, 2, 3, 4, 5, 6, 7, 8, 9]
     * true
     * [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
     * false
     * [21, 22, 23, 24, 25, 26, 27, 28, 29]
     * true
     * [30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
     * false
     * [41, 42, 43, 44, 45, 46, 47, 48, 49]
     * true
     * [50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
     * false
     * [61, 62, 63, 64, 65, 66, 67, 68, 69]
     * true
     * [70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80]
     * [81, 82, 83, 84, 85, 86, 87, 88, 89, 90]
     *
     * @return
     */
    private Map<Integer, List<Integer>> createNumbersByColumn() {
        // Generate random factor to set column bounds
        boolean[] includeLastNumber = new boolean[8];
        for (int i = 0; i < 8; i++) {
            includeLastNumber[i] = random.nextBoolean();
        }

        Map<Integer, List<Integer>> numbersByColumn = new HashMap<>();

        // First row start with 1, ends with either 9 or 10
        numbersByColumn.put(0, IntStream.range(1, includeLastNumber[0] ? 11 : 10).boxed().collect(Collectors.toList()));

        // Inside rows bounds change by the random factor generated
        for (int i = 1; i <= 7; i++) {
            numbersByColumn.put(1, IntStream.range(includeLastNumber[0] ? 11 : 10, includeLastNumber[1] ? 21 : 20).boxed().collect(Collectors.toList()));
            numbersByColumn.put(i, IntStream.range(includeLastNumber[i - 1] ? i * 10 + 1 : i * 10, includeLastNumber[i] ? i * 10 + 11 : i * 10 + 10).boxed().collect(Collectors.toList()));
        }

        // Last row starts with either 80 or 81, ends with 90
        numbersByColumn.put(8, IntStream.range(includeLastNumber[7] ? 81 : 80, 91).boxed().collect(Collectors.toList())); //

        return numbersByColumn;
    }

    /**
     * Get the id of rows with less than 5 numbers, i.e. they can accept more numbers. They will be used to swap
     *
     * @param numValuesInRow
     * @return
     */
    private List<Integer> getIdRowsWithAvailableSpace(int[] numValuesInRow) {
        List<Integer> idRowsWithAvailableSpace = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < numValuesInRow.length; rowIndex++) {
            if (numValuesInRow[rowIndex] < 5) {
                idRowsWithAvailableSpace.add(rowIndex);
            }
        }
        return idRowsWithAvailableSpace;
    }

    /**
     * Sort every ticket column in ascending order
     *
     * @param strip
     */
    private void sortColumns(int[][] strip) {
        for (int ticketIndex = 0; ticketIndex < NUM_TICKETS; ticketIndex++) {
            for (int colIndex = 0; colIndex < NUM_COLUMNS; colIndex++) {
                // Get a ticket column as list for easy manipulation
                List<Integer> ticketColumn = Arrays.asList(
                        strip[NUM_ROWS * ticketIndex][colIndex],
                        strip[NUM_ROWS * ticketIndex + 1][colIndex],
                        strip[NUM_ROWS * ticketIndex + 2][colIndex]);
                // Only 1 value, no need to sort, skip to next column
                if (ticketColumn.stream().filter(x -> x == 0).count() == 2) {
                    continue;
                }
                // Sort adjacent values
                ticketColumn.sort((o1, o2) -> {
                    if (o1 == 0 || o2 == 0) return 0;
                    return o1.compareTo(o2);
                });
                // If a zero in the middle the values might not be sorted, check and sort
                if (ticketColumn.stream().filter(x -> x == 0).count() == 1 &&
                        ticketColumn.get(1) == 0 &&
                        ticketColumn.get(0) > ticketColumn.get(2)) {
                    Collections.swap(ticketColumn, 0, 2);
                }
                // Add back sorted values
                strip[NUM_ROWS * ticketIndex][colIndex] = ticketColumn.get(0);
                strip[NUM_ROWS * ticketIndex + 1][colIndex] = ticketColumn.get(1);
                strip[NUM_ROWS * ticketIndex + 2][colIndex] = ticketColumn.get(2);
            }
        }
    }

    /**
     * Check if the column has other non-zero values, otherwise when we swap the value the column will have 3 zeroes, which is unacceptable.
     * This constrain causes about 1/10 of the generated strips to be dumped by firing a SwapUnavailableException. After catching the exception, the strip generation will be reattempted.
     *
     * @param strip
     * @param rowIndex
     * @param colIndex
     * @return
     */
    private boolean hasColumnOtherNonZeroValues(int[][] strip, int rowIndex, int colIndex) {
        int ticketRowIndex = rowIndex % 3;
        if (ticketRowIndex == 0) { // first row, check second and third for values
            return strip[rowIndex + 1][colIndex] != 0 && strip[rowIndex + 2][colIndex] != 0;
        } else if (ticketRowIndex == 1) { // second row, check above and below
            return strip[rowIndex - 1][colIndex] != 0 && strip[rowIndex + 1][colIndex] != 0;
        } else { // third row, check first and second
            return strip[rowIndex - 2][colIndex] != 0 && strip[rowIndex - 1][colIndex] != 0;
        }
    }

    /**
     * Print a simply formatted strip to the console
     *
     * @param strip
     * @param stripIndex
     */
    private void print(int[][] strip, int stripIndex) {
        String toWrite = "\tStrip n. " + (stripIndex + 1) + "\n\t___________________________________________________________________\n\n";
        for (int row = 0; row < strip.length; row++) {
            toWrite += "\t";
            for (int col = 0; col < strip[row].length; col++) {
                toWrite += strip[row][col] + "\t";
            }
            toWrite += "\t\n";
            if (row % 3 == 2) {
                toWrite += "\n";
            }
        }
        toWrite += "\t___________________________________________________________________\n";
        System.out.println(toWrite);
    }
}
