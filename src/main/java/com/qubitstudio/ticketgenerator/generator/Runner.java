package com.qubitstudio.ticketgenerator.generator;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Runner implements CommandLineRunner {


    @Override
    public void run(String... args) {
        int numStrips = args.length == 1 ? Integer.parseInt(args[0]) : 100;

        StripsGenerator stripsGenerator = new StripsGenerator();
        List<int[][]> strips = stripsGenerator.generate(numStrips, true);

        System.out.println(strips.size() + " strips generated");
    }

}
