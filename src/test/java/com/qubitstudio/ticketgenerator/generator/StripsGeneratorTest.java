package com.qubitstudio.ticketgenerator.generator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class StripsGeneratorTest {

    private static StripsGenerator stripsGenerator = new StripsGenerator();
    private final int NUM_TICKETS = 6;
    private final int NUM_ROWS = 3;
    private final int NUM_COLUMNS = 9;
    private final int NUM_STRIPS = 1000;

    @BeforeAll
    static void init() {

    }

    @Test
    void stripsGeneratedCorrect() {
        List<int[][]> strips = stripsGenerator.generate(NUM_STRIPS);
        assertEquals(NUM_STRIPS, strips.size());
    }

    @Test
    void ticketHas15Numbers() {
        List<int[][]> strips = stripsGenerator.generate(NUM_STRIPS);
        for (int[][] strip : strips) {
            for (int ticketIndex = 0; ticketIndex < NUM_TICKETS; ticketIndex++) {
                int countNumbers = 0;
                countNumbers += Arrays.stream(strip[ticketIndex * NUM_ROWS]).filter(n -> n != 0).count();
                countNumbers += Arrays.stream(strip[ticketIndex * NUM_ROWS + 1]).filter(n -> n != 0).count();
                countNumbers += Arrays.stream(strip[ticketIndex * NUM_ROWS + 2]).filter(n -> n != 0).count();
                assertEquals(15, countNumbers);
            }
        }
    }

    @Test
    void ticketColumnHasAtLeast1Number() {
        List<int[][]> strips = stripsGenerator.generate(NUM_STRIPS);
        for (int[][] strip : strips) {
            for (int ticketIndex = 0; ticketIndex < NUM_TICKETS; ticketIndex++) {
                for (int colIndex = 0; colIndex < NUM_COLUMNS; colIndex++) {
                    // Get a ticket column as list for easy manipulation
                    List<Integer> ticketColumn = Arrays.asList(
                            strip[NUM_ROWS * ticketIndex][colIndex],
                            strip[NUM_ROWS * ticketIndex + 1][colIndex],
                            strip[NUM_ROWS * ticketIndex + 2][colIndex]);
                    assertNotEquals(3, ticketColumn.stream().filter(x -> x == 0).count());
                }
            }
        }
    }

    @Test
    void numberInCorrectRange() {
        List<int[][]> strips = stripsGenerator.generate(NUM_STRIPS);
        for (int[][] strip : strips) {
            for (int rowIndex = 0; rowIndex < strip.length; rowIndex++) {
                for (int colIndex = 0; colIndex < strip[rowIndex].length; colIndex++) {
                    assertTrue(strip[rowIndex][colIndex] <= (colIndex + 1) * 10);
                    if (strip[rowIndex][colIndex] != 0) {
                        assertTrue(strip[rowIndex][colIndex] >= colIndex * 10);
                    }
                }
            }
        }

    }

    @Test
    void rowsHave5Numbers() {
        for (int[][] strip : stripsGenerator.generate(NUM_STRIPS)) {
            for (int[] row : strip) {
                int count = (int) Arrays.stream(row).filter(c -> c != 0).count();
                assertEquals(5, count);
            }
        }
    }

    @Test
    void stripHas90Numbers() {
        for (int[][] strip : stripsGenerator.generate(NUM_STRIPS)) {
            List<Integer> bingoStripNumbers = Arrays.stream(strip)
                    .flatMapToInt(Arrays::stream)
                    .filter(n -> n != 0)
                    .sorted()
                    .boxed()
                    .collect(Collectors.toList());

            assertEquals(90, bingoStripNumbers.size());
            assertEquals(IntStream.rangeClosed(1, 90).boxed().collect(Collectors.toList()), bingoStripNumbers);
        }
    }

    @Test
    void generate10ThousandStripsInLessThan1Second() {
        long startTime = System.nanoTime();
        stripsGenerator.generate(10000);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000; // milliseconds
        assertTrue(duration <= 1000);
    }


}
